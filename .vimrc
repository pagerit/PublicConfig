map <C-Tab> :bn
map <C-S-Tab> gT
set expandtab
autocmd FileType python set noexpandtab
autocmd FileType make set noexpandtab
set tabstop=2
set shiftwidth=2
set ai
set list
set number
set colorcolumn=80
set tw=579
set wrap linebreak nolist
set relativenumber
set titlestring=%f title
set mouse=nvch
set splitright
nmap <2-LeftMouse> a
map <c-c>  :vs %:r.
syntax on
