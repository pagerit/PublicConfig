#!/bin/sh

ln .bash ~ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .bash install failed or already installed" >&2
fi

ln .bash_aliases ~ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .bash_aliases install failed or already installed" >&2
fi

mkdir ~/.i3 2>/dev/null

ln .i3/*  ~/.i3/ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .i3 install failed or already installed" >&2
fi

ln .signature ~ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .signature install failed or already installed" >&2
fi
ln .slrnrc ~ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .slrnc install failed or already installed" >&2
fi
ln .vimrc ~ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .vimrc install failed or already installed" >&2
fi
ln .Xdefaults ~ 2>/dev/null
if [ $? -ne 0 ]; then
    echo "    .Xdefaults install failed or already installed">&2
fi

echo "\n\n"
echo "What do you want to install ?"
echo "  0 - Nothing"
echo "  1 - Just update and upgrade"
echo "  2 - 1 + basic utils"
echo "  3 - 2 + usefull programs"
echo "  4 - ALL"
echo "   (listInstall from more details)"

ans=7

while [ $ans -gt 5 ]; do
  echo -n "Specify a number (0-4) : "
  read ans
  case $ans in
    ''|*[!0-9]*) echo not a number
        ans=7 ;;
        *)  ;;
        esac
done
if [ $ans -gt 0 ]; then
  apt-get -y update
  apt-get -y upgrade
fi;
if [ $ans -gt 1 ]; then
  while IFS='' read -r line; do
     a=$(echo $line |cut -b 1)
     case $a in
        ''|*[!0-9]*) ;;
         *)
            if [ $a -eq 2 ]; then 
              echo  $(echo $line |cut -d ' ' -f 2)
            fi; 2>/dev/null
            ;;
        esac
  done < listInstall
fi;

if [ $ans -gt 2 ]; then
  while IFS='' read -r line; do
     a=$(echo $line |cut -b 1)
     case $a in
        ''|*[!0-9]*) ;;
         *)
            if [ $a -eq 3 ]; then 
              echo  $(echo $line |cut -d ' ' -f 2)
            fi; 2>/dev/null
            ;;
        esac
done < listInstall
fi;
if [ $ans -gt 3 ]; then
  while IFS='' read -r line; do
     a=$(echo $line |cut -b 1)
     case $a in
        ''|*[!0-9]*) ;;
         *)
            if [ $a -eq 4 ]; then 
              echo  $(echo $line |cut -d ' ' -f 2)
            fi; 2>/dev/null
            ;;
        esac
  done < listInstall
fi;

if [ $ans -gt 4 ]; then
  while IFS='' read -r line; do
     a=$(echo $line |cut -b 1)
     case $a in
        ''|*[!0-9]*) ;;
         *)
            if [ $a -eq 5 ]; then 
              echo  $(echo $line |cut -d ' ' -f 2)
            fi; 2>/dev/null
            ;;
        esac
  done < listInstall
fi;
