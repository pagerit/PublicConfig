TPPATH="/home/thib/cryptoapp/projet/cryptoappli/src"

W='\[\e[0m\]'
R='\[\e[0;91m\]'
G='\[\e[0;92m\]'
Y='\[\e[1;90m\]'
B='\[\e[1;94m\]'
P='\[\e[0;93m\]'
GREEN=$'\e[1;32m'
RED=$'\e[1;31m'
BLUE=$'\e[1;34m'
WHI=$'\e[m'


shopt -s checkwinsize # check  size term
shopt -s autocd
bind "set completion-ignore-case on"

export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export EDITOR=vim
export PAGER='less'
export NNTPSERVER=news.epita.fr
export HISTFILE=~/save/.bash_history


export PROMPT_COMMAND=prompt
function prompt() {
    local EXIT="$?"             # This needs to be first
    PS1=""
    PS1+="${B}\w ${Y}\t"
    echo -ne "\033]0;${PWD/$HOME/\~}\007"
    #echo -ne "\033]0;FIC 2018\007"
    if [ $EXIT != 0 ]; then
        PS1+="${R}${EXIT}" 
    else
        PS1+="${G}${EXIT}"
    fi
    PS1+="${P}\$ ${W}"
    if [ $(tput cols) -lt 70 ]; then
      PS1+="\n"
    fi;
}




function tp() {
  cd $TPPATH/$1
}

function tmp ()
{
  datejjmmaaahhmm=$(date +%d_%m_%Y_%Hh%M)
  if [ $# -ge 2 ]; then
    vim $1$datejjmmaaahhmm;
  else
    mkdir $1$datejjmmaaahhmm;
    cd $1$datejjmmaaahhmm
  fi;
}


function rtmp ()
{
  rm -r /tmp/*
}

function lgrep ()
{
  if [ $# -ge 3 ]; then
    grep --color=auto -n -r  -A $3 -B $2 "$1";
    return 0
  fi;
  if [ $# -ge 2 ]; then
   grep --color=auto -n -r  -C $2 "$1"
   return 0;
  fi;
   grep --color=auto -n -r "$1"
}


function md ()
{
  mkdir $1
  cd $1
}




alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias ls='ls --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


### WIFI depends on configuration ##
alias wifi='nmcli d wifi connect'
alias lwifi='sudo iw wlp2s0 scan | grep SSID'

alias m='make -j9'
alias mk='make -j9 check'

### FUN  ##
alias oui='echo -e "\e[33;1;5;44m" "\\^o^/"  "\e[0m"'
alias non='echo -e "\e[31;1;5;47m" "/-_-\\"  "\e[0m"'
alias peut-être='echo -e "\e[31;1;5;47m" "?#_#?"  "\e[0m"'
alias bzu='echo -e "\e[37;1;5;45m" "\^_^"  "\e[0m"'
alias bza='echo -e "\e[37;1;5;45m" "^_^/"  "\e[0m"'

alias bbash='vim ~/.bash'
alias sourc='source ~/.bash'
alias gdb='gdb -q'
alias firefox='firefox'
alias py='python3'
alias capture='ffmpeg -video_size 1920x1080 -framerate 25 -f x11grab -i :0.0 -f alsa -ac 2 -i hw:0 -strict experimental resolution.mp4'


alias t='cd ${TPPATH}'
alias src='cd ${TPPATH}/src'

alias cd..='cd ..'
alias ..='cd ..'

alias off='sudo poweroff'

### GIT ###
alias ga='git add'
alias gaa='make clean; git add src/*.c src/*.h Makefile AUTHORS TODO README'
alias gl='git shortlog'
alias gs='git status'
alias gp='git push origin master'
alias gc='git commit -m'
alias gpu='git pull origin --rebase'
alias gd='git diff'
alias gch='git checkout'
alias gla='git log --all --graph --pretty=format:"%h , %Cgreen%ar : %<|(25) %Cblue %s %>|(75) %C(yellow) %D %Cred %>|(105) %an"'
alias gf='git files'

function gi()
{
  echo "* pageri_t" > AUTHORS; 
  echo "########################## TODO ##########################" > TODO; 
  echo "######################### README #########################" > README;
  mkdir src;
  mkdir tests;
  echo "*.o" > .gitignore;
  git add AUTHORS;
  git add TODO;
  git add README
}

. ~/.bash_aliases
