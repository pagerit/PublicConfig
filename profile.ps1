###   Set-ExecutionPolicy -ExecutionPolicy RemoteSigned

# Find out if the current user identity is elevated (has admin rights)
$identity = [Security.Principal.WindowsIdentity]::GetCurrent()
$principal = New-Object Security.Principal.WindowsPrincipal $identity
$isAdmin = $principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

# If so and the current host is a command line, then change to red color 
# as warning to user that they are operating in an elevated context
if (($host.Name -match "ConsoleHost") -and ($isAdmin)) {
    $host.UI.RawUI.BackgroundColor = "DarkRed"
    $host.PrivateData.ErrorBackgroundColor = "White"
    $host.PrivateData.ErrorForegroundColor = "DarkRed"
    Clear-Host
}

# Useful shortcuts for traversing directories
function cd... { Set-Location ..\.. }
function cd.... { Set-Location ..\..\.. }

function prompt {
    $exitcode = $?
    if ($isAdmin) {
        "" + (Get-Date -f "HH:mm:ss") + (Get-Location)  # " 
    }
    else {
        if ($Host.Name -like '*ISE*') {
            "$env:USERNAME" + (Get-Date -f "HH:mm:ss") + (Get-Location) + "$exitcode $ "
        }
        else {
            "$([char]27)[31m $env:USERNAME$([char]27)[34m" + (Get-Date -f "HH:mm:ss") + "$([char]27)[36m" + (Get-Location) + "$exitcode $ "
        }
    }
}

$Host.UI.RawUI.WindowTitle = "PowerShell {0}" -f (Get-Location)
if ($isAdmin) {
    $Host.UI.RawUI.WindowTitle += "[ADMIN]"
}

# Simple function to start a new elevated process. If arguments are supplied then 
# a single command is started with admin rights; if not then a new admin instance
# of PowerShell is started.
function admin {
    if ($args.Count -gt 0) {   
        $argList = "& '" + $args + "'"
        Start-Process "$psHome\powershell.exe" -Verb runAs -ArgumentList $argList
    }
    else {
        Start-Process "$psHome\powershell.exe" -Verb runAs
    }
}

# Set UNIX-like aliases for the admin command, so sudo <command> will run the command
# with elevated rights. 
Set-Alias -Name su -Value admin
Set-Alias -Name sudo -Value admin


# Make it easy to edit this profile once it's installed
function bbash {
    if ($host.Name -match "ise") {
        $psISE.CurrentPowerShellTab.Files.Add($profile.CurrentUserAllHosts)
    }
    else {
        notepad $profile.CurrentUserAllHosts
    }
}

# We don't need these any more; they were just temporary variables to get to $isAdmin. 
# Delete them to prevent cluttering up the user profile. 
Remove-Variable identity
Remove-Variable principal

function la { param( $param1 ) Get-ChildItem -Name $param1 }

function ll { param( $param1 ) Get-ChildItem -Force $param1 }
function gs { git status }
function ga { param( $param1 ) git add $param1 }
function gco { param( $param1 ) git commit -m $param1 }
function gp { git push origin master }
function gpu { git pull origin --rebase }
function gla { git log --all --graph --pretty=format:"%h , %Cgreen%ar : %<|(25) %Cblue %s %>|(75) %C(yellow) %D %Cred %>|(105) %an" }
function gi {
    Write-Output "* pageri_t" > AUTHORS; 
    Write-Output "########################## TODO ##########################" > TODO; 
    Write-Output "######################### README #########################" > README;
    mkdir src;
    mkdir tests;
    Write-Output "*.o" > .gitignore;
    git add AUTHORS;
    git add TODO;
    git add README
}
